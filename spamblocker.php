<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

class SpamBlocker extends Module
{
    public function __construct()
    {
        $this->name = 'spamblocker';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'E-Com';
        $this->need_instance = 0;
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Spam Blocker');
        $this->description = $this->l('Remove prestashop spam from your back office.');
        $this->ps_versions_compliancy = ['min' => '1.6', 'max' => '1.6.99.99'];
    }

    public function install()
    {
        if (!parent::install()
            || !$this->registerHook('displayBackOfficeHeader')
        ) {
            return false;
        }
        return true;
    }

    public function getContent()
    {
        $output = '';
        $xmlPath = dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'xml'.DIRECTORY_SEPARATOR.'*.xml';
        $files = glob($xmlPath);
        $result = true;
        foreach ($files as $file) {
            $result &= unlink($file);
        }
        if ($result) {
            $output .= $this->displayConfirmation($this->l('Spam files deleted'));
        } else {
            $output .= $this->displayError($this->l('Error deleting spam files, please delete manually:').' '.$xmlPath);
        }
        return $output;
    }

    public function hookDisplayBackOfficeHeader()
    {
        if ($this->context->controller instanceof AdminThemesController) {
            $this->context->smarty->registerFilter('output', [$this, 'removeThemesBlock']);
        }
    }

    public function removeThemesBlock($output)
    {
        return preg_replace('| id="prestastore-content".*?</div>|ms', ' style="display:none" />', $output);
    }
}
